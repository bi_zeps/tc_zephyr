- [ZephyrToolchain](#zephyr-toolchain)
- [ZephyrWest](#zephyr-west)
- [ZephyrBase](#zephyr-base)

# Zephyr Toolchain

### 0.13.1-r0
* Initial version with SDK 0.13.1-r0
* Based on Zephyr West 0.11.1-r0

# Zephyr West

### 0.11.1-r0
* Initial version with west 0.11.1
* Based on Ubuntu 20.04

#  Zephyr Base

### 20.04-r0
* Initial version
* Based on Ubuntu 20.04
